section .data
decstring: db "-0123456789"

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall
    
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    push rbx
    mov rax, 0

    .loop:
    mov rbx, [rax+rdi]
    inc rax
    test rbx, 0xff
    jnz .loop
    dec rax
    pop rbx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rbx, 0
    mov rcx, 10

    .loop:
    inc rbx
    mov rdx, 0
    div rcx
    push rdx
    test rax, rax
    jnz .loop	

    .printloop:
    dec rbx
    pop rdx
    mov rdi, [decstring+rdx+1]
    call print_char
    test rbx,rbx
    jnz .printloop
    ret 

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi,rdi
    jns .uint
    push rdi
    mov di, [decstring]
    call print_char
    pop rdi
    neg rdi

    .uint:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rcx, 0

    .loop:
    mov rax, [rdi+rcx]
    mov rdx, [rsi+rcx]
    inc rcx
    cmp al, dl
    jne .not_equals

    .ifequals:
    cmp al, 0
    jg .loop
    mov rax, 1
    ret

    .not_equals:
    mov rax, 0
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rax
    and rax, 0xff
    cmp al, 0x0A
    jne .exit
    mov rax, 0

    .exit:
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rbx
    mov r10, rdi
    mov r11, rsi

    .read_firstchar:
    call read_char
    cmp al, 0x20
    jz .read_firstchar
    cmp al, 0x9
    jz .read_firstchar
    test rax, rax
    jz .exit
    mov rbx, 1
    mov [r10], al

    .read_nextchar:
    call read_char
    test rax, rax
    jz .exit
    cmp al, 0x20
    jz .exit
    cmp al, 0x9
    jz .exit
    mov [r10+rbx], rax
    inc rbx 
    dec r11
    test r11, r11
    jnz .read_nextchar

    .exit:
    mov rdx, rbx
    lea rax, [r10]
    pop rbx
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx
    mov rax, 0
    mov rcx, 0
    mov r10, 10

    .read_next_number:
    mov bl, byte[rdi+rcx]
    cmp bl, 0x30
    jl .exit
    cmp bl, 0x39
    jg .exit
    sub bl, 0x30
    mul r10
    add al, bl
    inc rcx
    jmp .read_next_number

    .exit:
    mov rdx, rcx
    pop rbx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov rcx, [rdi]
    cmp cl, 0x2d
    je .neg_int
    jmp parse_uint

    .neg_int:
    inc rdi
    call parse_uint
    neg rax
    inc rdx

    .exit:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    cmp rdx, 0
    je .buffer_overflow
    call string_length
    cmp rax, rdx
    jg .buffer_overflow
    mov rax, 0
    
    .char_copy:
    mov r10b,[rdi+rax]
    mov [rsi+rax], r10b
    inc rax
    cmp rax, rdx
    jne .char_copy
    ret

    .buffer_overflow:
    mov rax, 0
    ret
